﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VM
{
    public class CategoryVM
    {
        public int Id { get; set; }
        public string CategoryAr { get; set; }
        public string CategoryEn { get; set; }
        public string Img { get; set; }
        public int Type { get; set; }
    }
}
