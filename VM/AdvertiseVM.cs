﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VM
{
    public class AdvertiseVM
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int MainCatId { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }
        public Nullable<int> CategoryFK { get; set; }
        public string Category { get; set; }
        public Nullable<int> CityFk { get; set; }
        public string City { get; set; }
        public Nullable<int> Views { get; set; }
        public string Counter { get; set; }
        public string Model { get; set; }
        public Nullable<int> StatusFk { get; set; }
        public string InColor { get; set; }
        public string OutColor { get; set; }
        public string Condition { get; set; }
        public string Mobile { get; set; }
        public Nullable<bool> isMobile { get; set; }
        public Nullable<bool> isActive { get; set; }
        public string Img1 { get; set; }
        public string Img2 { get; set; }
        public string Img3 { get; set; }
        public string Img4 { get; set; }
        public string Img5 { get; set; }
        public string Img6 { get; set; }
        public string Img7 { get; set; }
        public string Img8 { get; set; }
        public double? Price { get; set; }
        public int AdvType { get; set; }
        public bool isFav { get; set; }
        public int MainCategory { get; set; }
    }
}
