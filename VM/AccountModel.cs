﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VM
{
    public class AccountModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Pass { get; set; }
        public string Mobile { get; set; }
        public string RegistrationId { get; set; }
        public int pr { get; set; }
        public string OldPass { get; set; }
       // public string Password { get; set; }
    }
}