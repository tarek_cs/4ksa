﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VM
{
    public class UserModel
    {
        public int UserId { get; set; }
        public int PlanId { get; set; }
        public string Name { get; set; }
        public int CoursesCount { get; set; }
        public int UserStatus { get; set; }
        public string UserMobile { get; set; }
        public int Pr { get; set; }
        public string Img { get; set; }
        public string Email { get; set; }
        public string Pname { get; set; }
        public string authKey { get; set; }

    }
}