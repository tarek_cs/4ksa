﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VM
{
    public class OfferVM
    {
        public int Id { get; set; }
        public string Offer { get; set; }
        public int Views { get; set; }
        public string Mobile { get; set; }
    }
}
