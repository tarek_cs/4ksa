﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL;
using VM;
using EF;
using System.Web;

namespace _4KSA.Controllers
{
    public class UsersController : ApiController
    {
        readonly LoginDAL _loginobj = new LoginDAL();
        [HttpGet]
        public UserModel Authenticate(string Mobile, string Pass, string RegistrationId)
        {
            AccountModel accounttb = new AccountModel { Mobile = Mobile, Pass = Pass, RegistrationId = RegistrationId };
            // successs login --> 1
            // password error --> -1 
            // error in server -->0
            // new user --> 2
            // int flag = 0;
            // -10 not confirm user
            int userId = 0;
            string usermobile = "";
            int pr = 0;
            string name = "";
            string userImg = "";
            string pname = "";
            string Email = "";

            int flag = _loginobj.Authenticate(accounttb, ref userId, ref usermobile, ref pr, ref name, ref userImg, ref pname,ref Email);
            //  int coursecount = _loginobj.GetCourseCount();
            UserModel usermodel = new UserModel
            {
                UserId = userId,
                Name = name,
                UserStatus = flag,
                UserMobile = usermobile,
                PlanId = pr,
                //Pr = pr,
                //Img = userImg,
                Email = Email,
                //Pname = pname,
                authKey = _loginobj.getMD5(userId + pr + _loginobj.tempKey)
            };
            return usermodel;
        }

        [HttpGet]
        public UserModel SignUp(string pass, string mobile, string RegistrationId)
        {
            AccountModel accounttb = new AccountModel
            {
                //  Name = name,
                //Email = mail,
                Pass = pass,
                Mobile = mobile,
                RegistrationId = RegistrationId,
                pr = 1
            };
            int userid = 0;
            string usermobile = "";
            //string profileimg = "http://koora.arabwin.com/areas/user-avatar-blue-96@2x.png";
            int flag = _loginobj.CreateNewLogin(accounttb, ref userid, ref usermobile);
            if (flag == 1)
            {
                UserModel usermodel = new UserModel
                {
                    UserId = userid,
                    //UserName = name,
                    UserStatus = 1,
                    UserMobile = usermobile,
                    Pr = accounttb.pr,
                    //Email = mail,
                    Pname = "",
                    // Img = profileimg,
                    authKey = _loginobj.getMD5(userid + accounttb.pr + _loginobj.tempKey)
                };
                return usermodel;
            }
            else
            {
                UserModel usermodel = new UserModel
                {
                    //UserName = name,
                    UserStatus = flag,
                    UserMobile = usermobile,
                    //Email = mail
                };
                return usermodel;
            }
        }
        [HttpGet]
        public bool CheckCode(string id, string textCode)
        {
            return _loginobj.CheckCode(id, textCode);
        }
        [HttpGet]
        public List<PlanTB> GetAllPlains()
        {
            return _loginobj.GetAllPlains();
        }
        [HttpGet]
        public bool ChoosePlan(int id, int plan)
        {
            return _loginobj.ChoosePlan(id, plan);
        }
        [HttpGet, HttpPost]
        public string UploadImage()
        {
            string uri = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/";
            var absoultpath = "";
            //var avatarabsoultpath = "";
            //var avartarfilepath = "";
            int id = int.Parse(HttpContext.Current.Request["id"]);
            //bool flag = false;
            try
            {
                if (HttpContext.Current.Request.Files["recFile"] != null)
                {
                    #region Upload File
                    HttpPostedFile file = HttpContext.Current.Request.Files["recFile"];
                    string tempNewFileName = DateTime.Now.ToString("yyyyMMddTHHmmss") + System.IO.Path.GetExtension(file.FileName);
                    string targetFilePath = HttpContext.Current.Server.MapPath("~/Areas") + "/" + tempNewFileName;
                    file.SaveAs(targetFilePath);
                    absoultpath = uri + "Areas/" + tempNewFileName;
                    #endregion
                    //HttpPostedFile fileavatar = HttpContext.Current.Request.Files["recFile"];
                    //string tempNewFileNameAvatar = "avatar" + DateTime.Now.ToString("yyyyMMddTHHmmss") + System.IO.Path.GetExtension(file.FileName);
                    //string targetFilePathavatar = HttpContext.Current.Server.MapPath("~/Areas") + "/" + tempNewFileNameAvatar;
                    //avatarabsoultpath = uri + "Areas/" + tempNewFileNameAvatar;
                    //avartarfilepath = "~/Areas/" + tempNewFileNameAvatar;
                    //fileavatar.SaveAs(targetFilePathavatar);
                }
                #region save data

                //// UserBLL _obj = new UserBLL();
                //if (absoultpath == "")
                //{
                //    absoultpath = uri + "Areas/player.png";
                //}
                // flag = _profileobj.UpdateUserImage(id, absoultpath, 1, avartarfilepath, avatarabsoultpath);
                #endregion
                return absoultpath;
                // return 0;
            }
            catch (Exception ex)
            {
                return "error";
            }
        }
        [HttpGet]
        public bool SaveRegistrationId(int userid, string regid)
        {
            return _loginobj.SaveRegistrationId(userid, regid);
        }
        [HttpGet]
        public bool EditProfile(int id, string mobile, string name, string email, string code)
        {
            return _loginobj.EditProfile(id,mobile,name,email,code);
        }
    }
}
