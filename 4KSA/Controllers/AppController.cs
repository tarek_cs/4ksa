﻿using EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL;

namespace _4KSA.Controllers
{
    public class AppController : ApiController
    {
        public  AboutTB GetAboutApp()
        {
            return AppDAL.GetAboutApp();
        }
        [HttpGet]
        public  bool SendMailFromContact(string User, string Email, string Msg)
        {
            return AppDAL.SendMailFromContact(User, Email, Msg);
        }
        [HttpGet]
        public bool RatingApp(int User, int rating )
        {
            return AppDAL.RatingApp(User, rating);
        }
    }
}

