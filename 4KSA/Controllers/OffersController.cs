﻿using DAL;
using EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VM;

namespace _4KSA.Controllers
{
    public class OffersController : ApiController
    {
        [HttpGet]
        public List<OfferVM> GetOffers(bool isToday = false, string listfilterstring = ",")
        {
            return OffersDAL.GetOffers(isToday,listfilterstring);
        }
        [HttpGet]
        public List<OfferVM> GetMyOffers(int id)
        {
            return OffersDAL.GetMyOffers(id);
        }
        [HttpGet]
        public bool IncreaseOfferView(int id)
        {
            return OffersDAL.IncreaseOfferView(id);
        }
        [HttpGet]
        public bool DeleteOffer(int id)
        {
            return OffersDAL.DeleteOffer(id);
        }
        [HttpPost]
        public int AddOffer(OfferTB offersTB)
        {
            return OffersDAL.AddOffer(offersTB);
        }
    }
}
