﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL;
using VM;
using EF;

namespace _4KSA.Controllers
{
    public class AdvertisementController : ApiController
    {
        [HttpGet]
        public  List<PageAdvertiseVM> GetPageAdvertise(int page=0)
        {
            return AdvertisementDAL.GetPageAdvertise(page);
        }
        [HttpGet]
        public  List<AdvertiseVM> GetAppAdvertise(int parent, int index=0,string listfilterstring=",", string search="")
        {
            return AdvertisementDAL.GetAppAdvertise(parent, index, listfilterstring, search);
        }
        [HttpGet]
        public  List<AdvertiseVM> GetMyAdvertise(int index, int sharedId)
        {
            return AdvertisementDAL.GetMyAdvertise(index,sharedId);
        }
        [HttpGet]
        public List<AdvertiseVM> GetFavoritesAdvertise(int index, int sharedId)
        {
            return AdvertisementDAL.GetFavoritesAdvertise(index, sharedId);
        }
        [HttpPost]
        public  int AddAdvertise(AdvertisementTB advertiseTb) {
            return AdvertisementDAL.AddAdvertise(advertiseTb);
        }
        [HttpGet]
        public  bool AddRemoveReports(int sharedId, int advertiseId, int isAdd = 1) {
            return AdvertisementDAL.AddRemoveReports(sharedId, advertiseId,isAdd);
        }
        [HttpGet]
        public  bool AddRemoveFavorites(int sharedId, int advertiseId, int isAdd = 1)
        {
            return AdvertisementDAL.AddRemoveFavorites(sharedId, advertiseId, isAdd);
        }
        [HttpGet]
        public bool DeleteAdvertise(int advId)
        {
            return AdvertisementDAL.DeleteAdvertise(advId);
        }
        [HttpGet]
        public bool ReAdvertise(int advId)
        {
            return AdvertisementDAL.ReAdvertise(advId);
        }
        public AdvertiseVM Getadvertisebyid(int item,int sharedId) {
            return AdvertisementDAL.Getadvertisebyid(item, sharedId);
        }
        public List<CityTB> GetAllCities()
        {
            return AdvertisementDAL.GetAllCities();
        }
         
    }
}
