﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL;
using VM;

namespace _4KSA.Controllers
{
    public class CategoryController : ApiController
    {
        [HttpGet]
        public List<CategoryVM> GetCategoryByParent(int parent = 0)
        {
            return CategoryDAL.GetCategoryByParent(parent);
        }

    }
}
