﻿myApp.onPageInit('subcategory', function (page) {
    // 1 Slide Per View, 50px Between
    var xname = $$("div[data-page='subcategory']").find('.navbar').find('.center');
    $$(xname[1]).html(subsharedname);
   // $$("#divsubname")
    var html = '';
    for (var i = 0; i < categoryB.length; i++) {
        html += '<div class="col-33 align-center"> <a href="#" data-id="' + categoryB[i].Id + '" onclick="openAdvertiseList(this);"> <img src="' + categoryB[i].Img + '" class="category-item" /> </a> <span class="category-text"> ' + categoryB[i].CategoryAr + ' </span></div>';
    }
    $$("#divcategoryb").html(html);
});

function openAdvertiseList(xthis) {
    lastIndex = 0;
    sharedAdvertise = $$(xthis).data("id");
    var parent = $$(xthis).data("id");
    myApp.showIndicator();
    $$.ajax({
        url: uri + 'api/Advertisement/GetAppAdvertise',
        type: 'GET',
        data: { parent: parent, index: lastIndex },
        success: function (data) {
            myApp.hideIndicator();
            data = JSON.parse(data);
            if (isEmpty(data)) {
                advList = [];
            } else {
                advList = data;
            }
            var mainView = myApp.addView('.view-main', {
                dynamicNavbar: true,
            });
            mainView.router.loadPage('advertise.html');
        },
        error: function () {
            //your error code
            msgbox("error please try again");
            myApp.hideIndicator();
        }
    });
}