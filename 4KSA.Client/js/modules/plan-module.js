﻿myApp.onPageInit('plan', function (page) {
    var html = '';
    for (var i = 0; i < planTB.length; i++) {
        html += '<div class="data-table">';
        html += "<table>";
        html += "                        <thead>";
        html += "                            <tr>";
        html += "                                <th><\/th>";
        html += "                                <th>" + planTB[i].Plan + "<\/th>";
        html += "                            <\/tr>";
        html += "                        <\/thead>";
        html += "                        <tbody>";
        html += "                            <tr>";
        html += "                                <td>" + planTB[i].Count + " اعلان<\/td>";
        html += "                                <td>" + planTB[i].Duration + " شهر<\/td>";
        html += "                            <\/tr>";
        html += "                            <tr>";
        html += "                                <td><a href=\"#\" data-id=\"" + planTB[i].Id + "\" class=\"button   color-green \" onclick=\"ChoosePlan(this);\">اختيار  <\/a> <\/td>";
        html += "                                <td> " + planTB[i].Price + " ر.س<\/td>";
        html += "                            <\/tr>";
        html += "                        <\/tbody>";
        html += "                    <\/table>";
        html += "                    <\/div>";

    }
    $$("#divplanitems").html(html);
});
function GetAllplans() {
    planTB = [];
    myApp.showIndicator();
    $$.ajax({
        url: uri + 'api/Users/GetAllPlains',
        type: 'GET',
        async: false,
        data: {},
        success: function (data) {
            myApp.hideIndicator();
            data = JSON.parse(data);
            if (isEmpty(data)) {
                planTB = [];
            } else {
                planTB = data;
                var mainView = myApp.addView('.view-main', {
                    dynamicNavbar: true,
                });
                mainView.router.loadPage('plan.html');
            }
        },
        error: function () {
            myApp.hideIndicator();
            msgbox("خطا في الاتصال مع الانترنت");
        }
    });
}
function ChoosePlan(xthis) {
    sharedPlan = $$(xthis).attr("data-id");
    myApp.showIndicator();
    $$.ajax({
        url: uri + 'api/Users/ChoosePlan',
        type: 'GET',
        async: false,
        data: { id: sharedId, plan: sharedPlan },
        success: function (data) {
            data = JSON.parse(data);
            myApp.hideIndicator();
            if (data == false) {
                msgbox("خطا في الاتصال مع الانترنت");
            } else {
                mainCatId = 0;
                 cityId = 0;
                 cityName = "";
                 catId = 0;
                 catName = "";
                 statusId = 0;
                 statusName = "";
                  addTitle = "";
                  addPrice = "";
                  addDesc = "";
                  addMobile = true;
                  addCounter = "";
                  addModel = "";
                  addIncolor = "";
                  addOutcolor = "";
                showNotification(" تم اختيار الباقه بنجاح   ");
                var mainView = myApp.addView('.view-main', {
                    dynamicNavbar: true,
                });
                mainView.router.loadPage('addadvertise.html');
                
            }
        },
        error: function () {
            myApp.hideIndicator();
            msgbox("خطا في الاتصال مع الانترنت");
        }
    });
}