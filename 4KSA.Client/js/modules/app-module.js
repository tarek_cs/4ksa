﻿myApp.onPageInit('about', function (page) {
    // 1 Slide Per View, 50px Between
    myApp.showIndicator();
    $$.ajax({
        url: uri + 'api/App/GetAboutApp',
        type: 'GET',
        data: {  },
        success: function (data) {
            myApp.hideIndicator();
            data = JSON.parse(data);
            $$("#lblapptext").html(data.Description);
        },
        error: function () {
            //your error code
            msgbox("حدث خطا برجاء المحاولة لاحقا");
            myApp.hideIndicator();
        }
    });
    
});
myApp.onPageInit('more', function (page) {

});

myApp.onPageInit('contact', function (page) {
    $$('#btncontactsend').on('click', function (e) {
        myApp.showIndicator();
        $$.ajax({
            url: uri + 'api/App/SendMailFromContact',
            type: 'GET',
            data: { User: $$("#txtsubject").val(), Mail: $$("#txtmail").val(), Msg: $$("#txtmsg").val() },
            success: function (data) {
                myApp.hideIndicator();
                data = JSON.parse(data);
                if (data == true) {
                    $$("#txtsubject").val('');
                    $$("#txtmsg").val('');
                    $$("#txtmail").val('');
                    msgbox("تم الارسال");
                }
                else {
                    msgbox(" حدث خطا برجاء المحاولة لاحقا");
                }
                //$$("#lblapptext").html(data.Description);
            },
            error: function () {
                //your error code
                msgbox("حدث خطا برجاء المحاولة لاحقا");
                myApp.hideIndicator();
            }
        });
    });
});

myApp.onPageInit('rating', function (page) {
    var itemCollection = $$(".rating span");
    for (var i = 0; i < itemCollection.length; i++) {
            $$(itemCollection[i]).find("i").attr("class", "fa fa-star-o");
            $$(itemCollection[i]).find("i").css("color", "#000");
    }
});
function RateApp(xthis) {
    var itemCollection = $$(".rating span");
    for (var i = 0; i < itemCollection.length; i++) {
        if (i < $$(xthis).attr("data-id")) {
            $$(itemCollection[i]).find("i").attr("class", "fa fa-star");
            $$(itemCollection[i]).find("i").css("color", "#f1c40f");
        }
        else {
            $$(itemCollection[i]).find("i").attr("class", "fa fa-star-o");
            $$(itemCollection[i]).find("i").css("color", "#000");
        }
    }
    var userid = sharedId || 0;
    myApp.showIndicator();
    $$.ajax({
        url: uri + 'api/App/RatingApp',
        type: 'GET',
        data: { User: userid, rating: $$(xthis).attr("data-id") },
        success: function (data) {
            myApp.hideIndicator();
            data = JSON.parse(data);
            if (data == true) {
                msgbox("تم ارسال التقييم");
            }
            else {
                msgbox(" حدث خطا برجاء المحاولة لاحقا");
            }
        },
        error: function () {
            //your error code
            msgbox("حدث خطا برجاء المحاولة لاحقا");
            myApp.hideIndicator();
        }
    });
}