﻿var slideList = [];
myApp.onPageInit('slider', function (page) {
    var html = '';
    for (var i = 0; i < slideList.length; i++) {
        html += '<div class="swiper-slide" style="background: url(' + slideList[i].Img + ') no-repeat center center ;background-size: 100% 100%;"><span></span></div>';
    }
    $$(".swiper-container").find(".swiper-wrapper").html(html);
    var mySwiper = myApp.swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationHide: false,
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
    });
});

function GoToSlider(xthis) {
    slideList = [];
    var lst = $$(xthis).parent("div.swiper-wrapper").find(".swiper-slide");
    for (var i = 0; i < lst.length; i++) {
        slideList.push({ Img: $$(lst[i]).attr("data-img")});
    }
    var mainView = myApp.addView('.view-main', {
        dynamicNavbar: true,
    });
    mainView.router.loadPage('slider.html');
}