﻿var planTB = [];
myApp.onPageInit('register', function (page) {
    $$("#signbtn").on('click', function () {
        Signup();
    });

});
myApp.onPageInit('confirm', function (page) {
    $$("#signcodebtn").on('click', function () {
        SendConfirmCode();
    });
});
function Signup() {
    //var name = $$("#signname").val();
    // var email = $$("#signemail").val();
    var mobile = $$("#signmobile").val();
    MDPass = md5(md5Salad + $$("#signepass").val());
    if (MDPass != '' && mobile != '') {
        mobile = $$("#ddlregisterkey").val() + mobile;
        sharedMobile = mobile;
        myApp.showIndicator();
        $$.ajax({
            url: uri + 'api/Users/SignUp',
            type: 'GET',
            data: { pass: MDPass, mobile: mobile, RegistrationId: regId },
            success: function (data) {
                data = JSON.parse(data);
                if (data.UserStatus == 1) {// save done
                    sharedId = data.UserId;
                    $$(".log-user").show();
                    authKey = data.authKey;
                    myApp.hideIndicator();
                    showNotification("تم ارسال رسالة برمز التحقق , قد تتأخر بضع دقائق");
                    var mainView = myApp.addView('.view-main', {
                        dynamicNavbar: true,
                    });
                    mainView.router.loadPage('confirm.html');
                }
                else if (data.UserStatus == -1) {
                    msgbox("الرجاء التاكد من صحه رقم الجوال ", "خطا ف ارسال كود التحقق");
                }
                else if (data.UserStatus == -2) {
                    msgbox("هذا المستخدم موجود بالفعل , الرجاء تسجيل الدخول ");
                }
                else {
                    myApp.hideIndicator();
                    msgbox("حدث خطا ما برجاء المحاوله لاحقا");
                }
                myApp.hideIndicator();
            },
            error: function () {
                myApp.hideIndicator();
                msgbox("خطأ في التسجيل , الرجاء اعادة المحاولة");
            }
        });
    } else {
        // $$("#signname").css("border-color", "red");
        $$("#signepass").css("border-color", "red");
        //$$("#signemail").css("border-color", "red");
        $$("#signmobile").css("border-color", "red");
    }
}
function checkMobile() {

    var mobile = $$("#signmobile").val();
    if (mobile.substring(0, 2) != '05') {
        $("#spnmobileerror").show();
    }
    else {
        $("#spnmobileerror").hide();
    }
}
function SendConfirmCode() {
    var codex = $$("#signcodetxt").val();
    if (codex != '' && codex != undefined) {
        myApp.showIndicator();
        $$.ajax({
            url: uri + 'api/Users/CheckCode',
            type: 'GET',
            async: false,
            data: { id: sharedMobile, textCode: codex },
            success: function (data) {
                myApp.hideIndicator();
                data = JSON.parse(data);
                if (data == false) {
                    msgbox("كود تفعيل خاطئ");
                } else {
                    // msgbox(" تم التفعيل   ");
                    GetAllplans();
                }
            },
            error: function () {
                myApp.hideIndicator();
                msgbox("خطا في الاتصال مع الانترنت");
            }
        });
    }
    else {
        $$("#signcodetxt").css("corder-color", "red")
    }
}

function logout() {
    LoggedIn = false;
    sharedId = 0;
    //userEmail = null;
    MDPass = null;
    sharedMobile = null;
    //sharedName = null;
    //sharedPr = null;
    //sharedImg = null;
    authKey = null;
    //$$("#spanUserName").html(sharedName);
    //$$("#imgUserAvatar").attr("src", sharedImg);
    try {
        writeToCache("ksacredentials", "n", false, function () {
        }, function () { showNotification("خطأ في التخزين !"); });
    }
    catch (err) {
    }
    mainView.router.loadPage('index.html');

}
myApp.onPageInit('login', function (page) {
    $$('#loginBtn').on('click', function () {
        userEmail =$$("#ddlregisterkey").val()+ $$('#txtlogmobile').val();
        var password = $$('#txtlogpass').val();;
        if (regId == '') {
            regId = 'null';
        }
        MDPass = md5(md5Salad + password);
        myApp.showIndicator();
        $$.ajax({
            url: uri + 'api/Users/Authenticate',
            type: 'GET',
            data: { Mobile: userEmail, Pass: MDPass, RegistrationId: regId },
            success: function (data) {
                data = JSON.parse(data);
                myApp.hideIndicator();
                if (isEmpty(data)) {

                } else {
                    sharedId = data.UserId;
                    $$(".log-user").show();
                    sharedMobile = data.UserMobile;
                    sharedPlan = data.PlanId;
                    authKey = data.authKey;
                    sharedName = data.Name;
                    sharedEmail = data.Email;
                    if (data.UserStatus == 1) { // logged in 
                        LoggedIn = true;
                        try {
                            writeToCache("ksacredentials", sharedId + "|" + MDPass + "|" + sharedMobile + "|" + authKey + "|" + sharedName + "|" + sharedEmail, false, function () {
                            }, function () { showNotification("خطأ في التخزين !"); });
                        }
                        catch (err) {
                        }
                        
                        var mainView = myApp.addView('.view-main', {
                            dynamicNavbar: true,
                        });
                        mainView.router.loadPage('addadvertise.html');
                    }
                    else if (data.UserStatus == -5) {
                        //LoggedIn = false;
                        GetAllplans();
                    }
                    else if (data.UserStatus == -1) {// open confrim popu
                        //popup-forgot
                        myApp.hideIndicator();
                        msgbox("الرجاء التأكد من الجوال  وكلمة المرور");
                    }
                    else if (data.UserStatus == -9) {// open confrim popu
                        //popup-forgot
                        myApp.hideIndicator();
                        msgbox("لقد تم حظر هذا الحساب من قبل الادمن");
                    }
                    else if (data.UserStatus == -10) {// open confrim popu                        
                        // userEmail = $("#txtusername").val();
                        //myApp.prompt('', ' ادخل كود التحقق', function (value) {
                        //    SendConfirmCode(value);
                        //});
                        var mainView = myApp.addView('.view-main', {
                            dynamicNavbar: true,
                        });
                        mainView.router.loadPage('confirm.html');
                    }
                    else {
                        myApp.hideIndicator();
                        msgbox("الرجاء التأكد من الجوال وكلمة المرور");
                        // add events
                    }
                }
            },
            error: function () {
                //your error code
                msgbox("حدث خطأ ما ! الرجاء إعادة المحاولة");
                myApp.hideIndicator();
            }
        });
    });
});
myApp.onPageInit('editprofile', function (page) {
    $$("#editmobile").val(sharedMobile.substring(4,sharedMobile.length));
    $$("#editname").val(sharedName);
    $$("#editemail").val(sharedEmail);
    $$("#editregisterkey").val(sharedMobile.substring(0, 4));
    $$('#btneditprofile').on('click', function () {
        editProfile('null');
    });
});

function editProfile(code) {
    var mobile = $$("#editmobile").val();
    if (mobile != '') {
        mobile = $$("#editregisterkey").val() + mobile;
        myApp.showIndicator();
        $$.ajax({
            url: uri + 'api/Users/EditProfile',
            type: 'GET',
            data: { id: sharedId, mobile: mobile, name: $$("#editname").val(), email: $$("#editemail").val(),code:code },
            success: function (data) {
                data = JSON.parse(data);
                if (data == true) {
                    if (code != 'null') {
                        msgbox("تم التعديل");
                        sharedMobile = mobile;
                        sharedName = $$("#editname").val();
                        sharedEmail = $$("#editemail").val();
                    }
                    else {
                            myApp.prompt(' تم ارساله للموبايل الخاص بك', 'الرقم التاكيدي', function (value) {
                                //myApp.alert('Your name is "' + value + '". You clicked Ok button');
                                editProfile(value);
                            });
                    }
                }
                else {
                    msgbox("خطأ في التسجيل , الرجاء اعادة المحاولة");
                }
                myApp.hideIndicator();
            },
            error: function () {
                myApp.hideIndicator();
                msgbox("خطأ في التسجيل , الرجاء اعادة المحاولة");
            }
        });
    } else {
        // $$("#signname").css("border-color", "red");
        $$("#editmobile").css("border-color", "red");
    }
}