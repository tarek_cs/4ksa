var offerTb = [];
var offerImg = "";
var offersFilterStr = ',';
var advertiseId = 0;
var offerMobile = "";
var offerViews = "";
$$('.btn-lang').on('click', function (e) {
    e.preventDefault();
    lang = $$(this).attr("data-id");
    // Initialize View          

    GetHomeAdvertise(2);
    GetCategory(0);
    // $$(this).addClass('hello').attr('title', 'world').insertAfter('.something-else');
});
myApp.onPageInit('home', function (page) {
    // 1 Slide Per View, 50px Between
    var htmlslider = '';
    for (var i = 0; i < slideData.length; i++) {
        htmlslider += '<div class="swiper-slide" onclick="GoToSlider(this);" data-img="' + slideData[i].Advertise + '" style="background: url(' + slideData[i].Advertise + ') no-repeat center center fixed ;background-size: 100% 100%;"><span></span></div>';
    }
    $$(".swiper-1").find(".swiper-wrapper").html(htmlslider);
    var html = '';
    for (var i = 0; i < categoryA.length; i++) {
        // html += '<div class="col-33 align-center"> <a href="#" onclick="openCategoryB(' + categoryA[i].Id + ');"> <img src="' + categoryA[i].Img + '" class="category-item" /> </a> <span class="category-text"> ' + categoryA[i].CategoryAr + ' </span></div>';
        html += '<div class="col-33" style=margin-top: 10px; margin-bottom:50px;> <a href="#" onclick="openCategoryB(this);" data-id="' + categoryA[i].Id + '" data-name="' + categoryA[i].CategoryAr + '"> <img src="' + categoryA[i].Img + '" class="category-item" /> </a><p>  ' + categoryA[i].CategoryAr + '</p> </div>';
    }
    $$("#divcategorya").html(html);
    var mySwiper1 = myApp.swiper('.swiper-1', {
        pagination: '.swiper-1 .swiper-pagination',
        spaceBetween: 50
    });



});
myApp.onPageInit('index', function (page) {
    var mainView = myApp.addView('.view-main', {
        dynamicNavbar: true,
    });
    if (mainView.activePage.name == 'index' || mainView.activePage.name == 'home') {
        $$(".tooltabbar").css("display", "none");
    }
    $$('.btn-lang').on('click', function (e) {
        e.preventDefault();
        lang = $$(this).attr("data-id");
        // Initialize View          

        GetHomeAdvertise(2);
        GetCategory(0);
        // $$(this).addClass('hello').attr('title', 'world').insertAfter('.something-else');
    });
});
myApp.onPageInit('offers', function (page) {
    var htmlcat = '';
    htmlcat += "<span class='filter-lastcatAA' data-id='" + 0 + "' onclick='selectFilterList(this);'><span class='text1' >الكل</span>";
    htmlcat += "<img class='img-valign' style'margin-bottom: 3px;' src='img/dots.png' alt=''></span>&nbsp;";
    for (var i = 0; i < categoryA.length; i++) {
        htmlcat += "<span class='filter-lastcatAA' data-id='" + categoryA[i].Id + "'  onclick='selectFilterListA(this);'><span class='text1' >" + categoryA[i].CategoryAr + "</span>";
        htmlcat += "<img class='img-valign' style'margin-bottom: 3px;' height='50' width='50' src='" + categoryA[i].Img + "' alt=''></span>&nbsp;";
    }
    $$("#divoffersFilter").html(htmlcat);
    offersFilterStr = ',';
    LoadOffers();
});
myApp.onPageInit('offersImage', function (page) {
    var html = '<div style="padding:0px; padding-bottom: 50px;" id="divimgOffer"><img src="' + offerImg + '" style="text-align:center; height:475px; width:100%" /><div style="background-color: #339966; text-align:center;"><div class="row" style="text-align:center;"><div class="col-25" style="color:white; margin-top:5px;">' + offerViews + ' مشاهدة </div><div class="col-25" style="margin-top: 10px;" onclick="capturePhoto();"><img src="img/share2.png"/></div><div class="col-25" style="margin-top: 10px;"><img src="img/coughs.png"/></div><div class="col-25" style="margin-top: 10px;" onclick="CallMobileOffer(' + offerMobile + ');"><img src="img/call2.png"/></div></div></div></div>';
    $$("#divofferimg").html(html);
});
myApp.onPageInit('todayoffer', function (page) {
    var html = '';
    for (var i = 0; i < offerTb.length; i++) {
        html += '<div class="swiper-slide" style="background: url(http://localhost:63539/img/lock.jpg) no-repeat center center ;background-size: 100% 100%;"><span></span></div>';
    }
    $$(".swiper-container").find(".swiper-wrapper").html(html);
    var mySwiper = myApp.swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationHide: false,
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
    });
});
function GetHomeAdvertise(pageIndex) {
    //myApp.showIndicator();
    $$.ajax({
        url: uri + 'api/Advertisement/GetPageAdvertise',
        type: 'GET',
        data: { page: pageIndex },
        success: function (data) {
            // myApp.hideIndicator();
            data = JSON.parse(data);
            if (isEmpty(data)) {
                if (pageIndex == 2)
                    slideData = [];
            } else {
                if (pageIndex == 2)//home page
                    slideData = data;
            }
        },
        error: function () {
            //your error code
            msgbox("error please try again");
            // myApp.hideIndicator();
        }
    });
}
function GetCategory(parentIndex) {
    myApp.showIndicator();
    $$.ajax({
        url: uri + 'api/Category/GetCategoryByParent',
        type: 'GET',
        data: { parent: parentIndex },
        success: function (data) {
            myApp.hideIndicator();
            data = JSON.parse(data);
            if (isEmpty(data)) {
                categoryA = [];
            } else {
                categoryA = data;
            }
            var mainView = myApp.addView('.view-main', {
                dynamicNavbar: true,
            });
            mainView.router.loadPage('home.html');
            $$(".tooltabbar").css("display", "");
        },
        error: function () {
            //your error code
            msgbox("error please try again");
            myApp.hideIndicator();
        }
    });
}
var subsharedname = '';
function openCategoryB(xthis) {
    subsharedname = $$(xthis).attr("data-name");
    sharedMainCategory = $$(xthis).attr("data-id");
    myApp.showIndicator();
    $$.ajax({
        url: uri + 'api/Category/GetCategoryByParent',
        type: 'GET',
        data: { parent: sharedMainCategory },
        success: function (data) {
            myApp.hideIndicator();
            data = JSON.parse(data);
            if (isEmpty(data)) {
                categoryB = [];
            } else {
                categoryB = data;
            }
            var mainView = myApp.addView('.view-main', {
                dynamicNavbar: true,
            });
            mainView.router.loadPage('subcategory.html');
        },
        error: function () {
            //your error code
            msgbox("error please try again");
            myApp.hideIndicator();
        }
    });
}
$$('.tab-icon').on('click', function (e) {
    e.preventDefault();
    $$(".tab-icon").removeClass("active");
    $$(this).addClass("active");
    // Initialize View          
    var mainView = myApp.addView('.view-main')
    var active_page = mainView.activePage; 
    var xpage= active_page.name+".html";
    if (xpage != $$(this).attr("href")) {
        if ($$(this).attr("href") == "offers.html") {
            GetOffers(false);
        }
        else if ($$(this).attr("href") == "todayoffer.html") {
            openToday();
        }
        else if ($$(this).attr("href") == "addadvertise.html") {
            openAddAdvertise();
        }
        else {
            var mainView = myApp.addView('.view-main', {
                dynamicNavbar: true,
            });
            mainView.router.loadPage($$(this).attr("href"));
        }
    }
    else {

    }
    // $$(this).addClass('hello').attr('title', 'world').insertAfter('.something-else');
});
function GetOffers(isToday,rout) {
    offerTb = [];
    myApp.showIndicator();
    $$.ajax({
        url: uri + 'api/Offers/GetOffers',
        type: 'GET',
        data: { isToday: isToday, listfilterstring: offersFilterStr },
        success: function (data) {
            myApp.hideIndicator();
            data = JSON.parse(data);
            if (isEmpty(data)) {
                offerTb = [];
            } else {
                offerTb = data;
            }
            var mainView = myApp.addView('.view-main', {
                dynamicNavbar: true,
            });
            if (rout == undefined) {
                if (isToday) {
                    mainView.router.loadPage('todayoffer.html');
                }
                else {
                    mainView.router.loadPage('offers.html');
                }
            }
            else {
                LoadOffers();
            }
        },
        error: function () {
            //your error code
            msgbox("error please try again");
            myApp.hideIndicator();
        }
    });
}
function openOffer(xthis) {
    offerViews=$$(xthis).attr("data-views");
    offerMobile = $$(xthis).attr("data-mobile");
    offerImg = $$(xthis).attr("data-img");
    var mainView = myApp.addView('.view-main', {
        dynamicNavbar: true,
    });
    mainView.router.loadPage('offersImage.html');
}
function openToday() {
    GetOffers(true);
}
function openAddAdvertise() {
    if (sharedId == 0) {
        //go to regiset page
        var mainView = myApp.addView('.view-main', {
            dynamicNavbar: true,
        });
        mainView.router.loadPage('register.html');
    }
    else {
        mainCatId = 0;
        cityId = 0;
        cityName = "";
        catId = 0;
        catName = "";
        statusId = 0;
        statusName = "";
        addTitle = "";
        addPrice = "";
        addDesc = "";
        addMobile = true;
        addCounter = "";
        addModel = "";
        addIncolor = "";
        addOutcolor = "";
        advertiseId = 0;
        img1 = "";
        img2 = "";
        img3 = "";
        img4 = "";
        img5 = "";
        img6 = "";
        img7 = "";
        img8 = "";
        var mainView = myApp.addView('.view-main', {
            dynamicNavbar: true,
        });
        mainView.router.loadPage('addadvertise.html');
    }
}
function selectFilterListA(xthis) {
    if ($$(xthis).css("color") == 'rgb(0, 0, 0)') {
        if ($$(xthis).attr('data-id') == 0) {
            $$(".filter-lastcatA").css("color", "");
            $$(xthis).css("color", "#f1c40f");
            offersFilterStr = ',';
        }
        else {
            if (offersFilterStr == ',') {
                //offersFilterStr = '';
                offersFilterStr = "," + $$(xthis).attr('data-id') + ',';
                $$(".filter-lastcatA[data-id='0']").css("color", "");
                $$(xthis).css("color", "#f1c40f");
            }
            else {
                offersFilterStr += $$(xthis).attr('data-id') + ",";
                $$(".filter-lastcatA[data-id='0']").css("color", "");
                $$(xthis).css("color", "#f1c40f");
            }
        }
    }
    else {
        if ($$(xthis).attr('data-id') == 0) {

        }
        else {
            $$(xthis).css("color", "#000000")
            var cutValue = $$(xthis).attr('data-id') + ",";
            offersFilterStr = offersFilterStr.replace(cutValue, "")
        }
    }
    lastIndex = 0;
    GetOffers(false,0);
   // LoadOffers();
}
function LoadOffers() {
    var html = '';
    for (var i = 0; i < offerTb.length; i++) {
        html += '<div class="col-50 align-center"> <a href="#" data-img=' + offerTb[i].Offer + ' data-mobile="' + offerTb[i].Mobile + '" data-views="' + offerTb[i].Views + '" onclick="openOffer(this);"> <img src="' + offerTb[i].Offer + '" style="text-align:center;height:300px;width:95%; border: 1px solid lightgrey;" /> </a></div>';
    }
    $$("#divalloffers").html(html);
}
function CallMobileOffer(mobile) {
    var number = mobile;
    var bypassAppChooser = true;
    window.plugins.CallNumber.callNumber(onmobileSuccess, onmobileError, number, bypassAppChooser);
}
function onmobileSuccess(result) {
    console.log("Success:" + result);
}

function onmobileError(result) {
    console.log("Error:" + result);
}