﻿/// <reference path="AWSFCordova.js" />
/// <reference path="general.js" />

var map;

document.addEventListener("deviceready", onDeviceReady, false);
document.addEventListener("menubutton", function () {
    //if loggedin , show left panel    
}, true);
document.addEventListener("backbutton", function (e) {
    goBack();
    e.preventDefault();
}, false);


function onDeviceReady() {
    navigator.splashscreen.show();
    myApp.showIndicator();
    setTimeout(function () {
        myApp.hideIndicator();
        navigator.splashscreen.hide();
    }, 2000);
    // $(".statusbar-overlay").css("background", "transparent");
    try {
        readFromCache("ksacredentials", function (fileresponse) {
            if (fileresponse == '' || fileresponse == 'n') {
                myApp.init();
                InitNotification();
                return;
            }
            InitNotification();
            var scredentials = fileresponse.split("|");
            sharedId = scredentials[0];
            MDPass = scredentials[1];
            sharedMobile = scredentials[2];
            authKey = scredentials[3];
            sharedName = scredentials[4];
            myApp.init();
            myApp.showIndicator();
            $$.ajax({
                url: uri + 'api/Users/Authenticate',
                type: 'GET',
                data: { Mobile: sharedMobile, Pass: MDPass, RegistrationId: regId },
                success: function (data) {
                    data = JSON.parse(data);
                    myApp.hideIndicator();
                    if (isEmpty(data)) {

                    } else {
                        if (data.UserStatus == 1) { // logged in                        
                            sharedId = data.UserId;
                            $$(".log-user").show();
                            sharedMobile = data.UserMobile;
                            sharedPlan = data.PlanId;
                            authKey = data.authKey;
                            sharedName = data.Name;
                            sharedId = data.UserId;
                            sharedEmail = data.Email;
                            LoggedIn = true;
                        }
                        else {
                            sharedId = 0;
                            $$(".log-user").hide();
                            sharedMobile = "";
                            sharedPlan = 0;
                            authKey = "";
                            MDPass = "";
                            LoggedIn = false;
                        }
                    }
                },
                error: function () {
                    msgbox("حدث خطأ ما ! الرجاء إعادة المحاولة");
                    myApp.hideIndicator();
                }
            });
        }, function (e) {
            myApp.init();
        });

    }
    catch (e) {
        //alert("#8" + e.toString());
        myApp.init();
        //mainView = myApp.addView('.view-main');
        //mainView.router.loadPage('welcome.html');
    }
    // end push notification        

}

function InitNotification() {
    var SenderId = "";
    var deviceType = (navigator.userAgent.match(/iPad/i)) == "iPad" ? "iPad" : (navigator.userAgent.match(/iPhone/i)) == "iPhone" ? "iPhone" : (navigator.userAgent.match(/Android/i)) == "Android" ? "Android" : (navigator.userAgent.match(/BlackBerry/i)) == "BlackBerry" ? "BlackBerry" : "null";
    if (deviceType == "iPhone" || deviceType == "iPad") {
        SenderId = "";
    }
    else {
        SenderId = "105742466625";
    }
    window.plugins.OneSignal.startInit("cf926a99-6a39-4a4c-8409-15d813d6f305", SenderId)
   .handleNotificationOpened(notificationOpenedCallback)
   .endInit();
    window.plugins.OneSignal.registerForPushNotifications();
    //push one signal
    var notificationOpenedCallback = function (jsonData) {
        //console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    };
    window.plugins.OneSignal.getIds(function (ids) {
        regId = ids.userId;
        //alert(regId);
        if (sharedId != 0 && sharedId != '' && sharedId != null) {
            $.ajax({
                url: uri + 'api/Users/SaveRegistrationId',
                type: 'GET',
                data: { userid: sharedId, regid: regId },
                success: function (data) {
                    //alert(data);
                },
                error: function () {
                    // alert("حدث خطأ ما ! الرجاء إعادة المحاولة");
                }
            });
        }
    });
}