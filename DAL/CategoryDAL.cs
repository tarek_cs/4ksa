﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VM;
using EF;

namespace DAL
{
    public static class CategoryDAL
    {
        public static List<CategoryVM> GetCategoryByParent(int parent = 0)
        {
            using (var db = new KsaDBEntities())
            {
                List<CategoryVM> lst = new List<CategoryVM>();
                var collection = db.CategoryTBs.Where(i => i.ParentId == parent);
                int j = 0;
                int x = 0;
                foreach (var item in collection)
                {
                    j++;
                    lst.Add(new CategoryVM() { Id = item.Id, CategoryAr = item.CategoryAr, CategoryEn = item.CategoryEn, Img = item.Img, Type = 0 });
                    if (j == 6 && parent == 0)
                    {
                        var data = db.PageAdvertiseTBs.Where(p => p.PageId == 4).OrderByDescending(c => c.Id).Skip(x).Take(1).FirstOrDefault();
                        lst.Add(new CategoryVM() { Id = 0, Img = data != null ? data.Advertise : "img/banner-placeholder.jpg", Type = 1 });
                        x++;
                        j = 0;
                    }
                }
                return lst;
            }
        }
    }
}
