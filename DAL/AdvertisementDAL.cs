﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VM;
using EF;
using System.Data.Entity;

namespace DAL
{
    public static class AdvertisementDAL
    {
        public static List<PageAdvertiseVM> GetPageAdvertise(int page)
        {
            using (var db = new KsaDBEntities())
            {
                List<PageAdvertiseVM> lst = new List<PageAdvertiseVM>();
                var collection = db.PageAdvertiseTBs.Where(i => i.PageId == page);
                foreach (var item in collection)
                {
                    lst.Add(new PageAdvertiseVM() { Id = item.Id, Advertise = item.Advertise });
                }
                return lst;
            }
        }
        public static List<AdvertiseVM> GetAppAdvertise(int parent, int index, string listfilterstring, string search)
        {
            List<AdvertiseVM> lst = new List<AdvertiseVM>();
            using (var db = new KsaDBEntities())
            {
                try
                {
                    if (listfilterstring == ",")
                    {
                        listfilterstring = "-1";
                    }
                    List<GetAppAdvertise_Result> collection = new List<GetAppAdvertise_Result>();
                    int startPage = 4 * index;
                    if (search == null)
                        search = "";
                    collection = db.GetAppAdvertise(listfilterstring, parent, search).OrderByDescending(x => x.Id).Skip(startPage).Take(4).ToList();
                    foreach (var item in collection)
                    {
                        lst.Add(new AdvertiseVM() { Id = item.Id, Title = getFirst50Char(item.Description), Date = item.Date.Value.ToShortDateString(), Price = item.Price, Img1 = item.Img1 != null ? item.Img1 : "img/placeholder.jpg", City = item.City, AdvType = item.AdvType });
                    }
                    var advertiseData = db.PageAdvertiseTBs.Where(i => i.PageId == 3).OrderByDescending(x => x.Id).Skip(index).Take(1).FirstOrDefault();
                    if (advertiseData != null)
                        lst.Add(new AdvertiseVM() { Img1 = advertiseData.Advertise, AdvType = 1 });
                    else
                        lst.Add(new AdvertiseVM() { Img1 = "img/banner-placeholder.jpg", AdvType = 1 });
                }
                catch (Exception ex)
                {


                }
                return lst;
            }
        }
        public static string getFirst50Char(string myString)
        {
            if (myString.Length > 0)
            {
                myString = myString.Substring(0, 50);
                int countSpaces = myString.Count(Char.IsWhiteSpace); // 6
                myString = myString.Substring(0, (50 - countSpaces)) + "...";
            }
            else
            {
                myString = myString.Substring(0, myString.Length) + "...";
            }

            return myString;
        }
        public static List<AdvertiseVM> GetMyAdvertise(int index, int sharedId)
        {
            List<AdvertiseVM> lst = new List<AdvertiseVM>();
            using (var db = new KsaDBEntities())
            {
                try
                {
                    int startPage = 4 * index;

                    var collection = db.AdvertisementTBs.Where(i => i.UserId == sharedId).OrderByDescending(x => x.Id).Skip(startPage).Take(4).ToList();
                    foreach (var item in collection)
                    {
                        lst.Add(new AdvertiseVM() { Id = item.Id, Title = item.Description.Length > 50 ? item.Description.Substring(0, 50) + "..." : item.Description.Substring(0, item.Description.Length) + "...", Date = item.Date.Value.ToShortDateString(), Price = item.Price, Img1 = item.Img1 != null ? item.Img1 : "img/placeholder.jpg", City = item.CityTB.City, isActive = item.isActive });
                    }

                }
                catch (Exception ex)
                {


                }
                return lst;
            }
        }

        public static List<AdvertiseVM> GetFavoritesAdvertise(int index, int sharedId)
        {
            List<AdvertiseVM> lst = new List<AdvertiseVM>();
            using (var db = new KsaDBEntities())
            {
                try
                {
                    int startPage = 4 * index;

                    var collection = db.AdvertisementTBs.Where(i => i.FavoritesTBs.Any(x => x.UserId == sharedId) && i.isActive == true).OrderByDescending(x => x.Id).Skip(startPage).Take(4).ToList();
                    foreach (var item in collection)
                    {
                        lst.Add(new AdvertiseVM() { Id = item.Id, Title = item.Title, Date = item.Date.Value.ToShortDateString(), Price = item.Price, Img1 = item.Img1 != null ? item.Img1 : "img/placeholder.jpg", City = item.CityTB.City, MainCatId = item.CategoryTB.MainCategory });
                    }
                }
                catch (Exception ex)
                {


                }
                return lst;
            }
        }
        public static AdvertiseVM Getadvertisebyid(int item, int sharedId)
        {
            using (var db = new KsaDBEntities())
            {

                var data = db.AdvertisementTBs.Find(item);
                AdvertiseVM obj = new AdvertiseVM()
                {
                    Id = data.Id,
                    City = data.CityTB.City,
                    MainCatId = data.CategoryTB.MainCategory,
                    Counter = data.Counter,
                    Category = data.CategoryTB.CategoryAr,
                    CategoryFK = data.CategoryFK,
                    CityFk = data.CityFk,
                    StatusFk = data.StatusFk,
                    Date = data.Date.Value.ToShortDateString(),
                    Img1 = data.Img1 != null ? data.Img1 : "img/placeholder.jpg",
                    Img2 = data.Img2,
                    Img3 = data.Img3,
                    Img4 = data.Img4,
                    Img5 = data.Img5,
                    Img6 = data.Img6,
                    Img7 = data.Img7,
                    Img8 = data.Img8,
                    Title = data.Title,
                    Description = data.Description,
                    InColor = data.InColor,
                    OutColor = data.OutColor,
                    isMobile = data.isMobile,
                    Model = data.Model,
                    Price = data.Price,
                    Views = data.Views,
                    Condition = data.StatusTB.Status,
                    Mobile = data.UsersTB.Mobile,
                    isFav = sharedId != 0 ? data.FavoritesTBs.Any(x => x.AdvertiseId == item && x.UserId == sharedId) : false
                };
                var str = Task.Run<bool>(() => IncrementAdvertise((int)item));
                return obj;
            }
        }
        public static List<CityTB> GetAllCities()
        {
            using (var db = new KsaDBEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var data = db.CityTBs.ToList();
                return data;
            }
        }
        public static int AddAdvertise(AdvertisementTB advertiseTb)
        {
            try
            {
                using (var db = new KsaDBEntities())
                {
                    if (advertiseTb.Img1 == null || advertiseTb.Img1 == "")
                    {
                        advertiseTb.Img1 = "img/placeholder.jpg";
                    }
                    if (advertiseTb.Title == "" || advertiseTb.Title == null)
                    {
                        advertiseTb.Title = advertiseTb.Description.Length > 50 ? advertiseTb.Description.Substring(0, 30) + "..." : advertiseTb.Description.Substring(0, advertiseTb.Description.Length) + "...";
                    }
                    advertiseTb.Date = DateTime.Now;
                    advertiseTb.StatusFk = advertiseTb.StatusFk == 0 ? null : advertiseTb.StatusFk;
                    advertiseTb.CategoryFK = advertiseTb.CategoryFK == 0 ? null : advertiseTb.CategoryFK;
                    advertiseTb.CityFk = advertiseTb.CityFk == 0 ? null : advertiseTb.CityFk;
                    advertiseTb.isActive = true;
                    if (advertiseTb.Id == 0) //add 
                    {
                        db.AdvertisementTBs.Add(advertiseTb);
                        db.SaveChanges();
                    }
                    else //edit
                    {
                        db.Entry(advertiseTb).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    return advertiseTb.Id;
                }
            }
            catch (Exception)
            {

                return 0;
            }
        }
        public static async Task<bool> IncrementAdvertise(int item)
        {
            try
            {
                using (var db = new KsaDBEntities())
                {
                    var data = db.AdvertisementTBs.Find(item);
                    if (data.Views != null)
                    {
                        data.Views = data.Views + 1;
                    }
                    else
                    {
                        data.Views = 1;
                    }
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static bool AddRemoveFavorites(int sharedId, int advertiseId, int isAdd = 1)
        {
            try
            {
                using (var db = new KsaDBEntities())
                {
                    var favCollection = db.FavoritesTBs.Where(i => i.UserId == sharedId && i.AdvertiseId == advertiseId);
                    if (isAdd == 1)
                    {
                        if (favCollection.Count() == 0)
                        {
                            FavoritesTB faveTb = new FavoritesTB() { Id = 0, UserId = sharedId, AdvertiseId = advertiseId };
                            db.FavoritesTBs.Add(faveTb);
                            db.SaveChanges();
                        }
                        return true;
                    }
                    else
                    {
                        db.FavoritesTBs.RemoveRange(favCollection);
                        db.SaveChanges();
                        return true;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool AddRemoveReports(int sharedId, int advertiseId, int isAdd = 1)
        {
            try
            {
                using (var db = new KsaDBEntities())
                {
                    var repCollection = db.ReportTBs.Where(i => i.UserId == sharedId && i.AdvertiseId == advertiseId);
                    if (isAdd == 1)
                    {
                        if (repCollection.Count() == 0)
                        {
                            ReportTB repTb = new ReportTB() { Id = 0, UserId = sharedId, AdvertiseId = advertiseId };
                            db.ReportTBs.Add(repTb);
                            db.SaveChanges();
                        }
                        return true;
                    }
                    else
                    {
                        db.ReportTBs.RemoveRange(repCollection);
                        db.SaveChanges();
                        return true;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool DeleteAdvertise(int advId)
        {
            try
            {
                using (var db = new KsaDBEntities())
                {
                    var data = db.AdvertisementTBs.Find(advId);
                    data.isActive = false;
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool ReAdvertise(int advId)
        {
            try
            {
                using (var db = new KsaDBEntities())
                {
                    var data = db.AdvertisementTBs.Find(advId);
                    data.isActive = true;
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
