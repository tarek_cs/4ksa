﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF;
using VM;
using System.Data.Entity;

namespace DAL
{
    public static class OffersDAL
    {
        public static List<OfferVM> GetOffers(bool isToday = false, string listfilterstring = ",")
        {
            List<OfferVM> lst = new List<OfferVM>();
            using (var db = new KsaDBEntities())
            {
                if (listfilterstring == ",")
                {
                    listfilterstring = "-1";
                }
                var collection = db.GetOffersByCategory(listfilterstring, isToday).ToList();
                foreach (var item in collection)
                {
                    lst.Add(new OfferVM() { Id = item.Id, Offer = item.Offer, Views = item.Views != null ? (int)item.Views : 0, Mobile = item.Mobile });
                }
                return lst;
            }
        }
        public static List<OfferVM> GetMyOffers(int id)
        {
            List<OfferVM> lst = new List<OfferVM>();
            using (var db = new KsaDBEntities())
            {

                var collection = db.OfferTBs.Where(i => i.UserId == id && i.isActive == true).OrderByDescending(i=> i.Id);
                foreach (var item in collection)
                {
                    lst.Add(new OfferVM() { Id = item.Id, Offer = item.Offer, Views = item.Views != null ? (int)item.Views : 0, Mobile = item.Mobile });
                }
                return lst;
            }
        }

        public static bool IncreaseOfferView(int id)
        {
            try
            {
                using (var db = new KsaDBEntities())
                {
                    var data = db.OfferTBs.Find(id);
                    data.Views = data.Views + 1;
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool DeleteOffer(int id)
        {
            try
            {
                using (var db = new KsaDBEntities())
                {
                    var data = db.OfferTBs.Find(id);
                    data.isActive = false;
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static int AddOffer(OfferTB offerTb)
        {
            try
            {
                using (var db = new KsaDBEntities())
                {
                    offerTb.isActive = true;
                    offerTb.Views = 0;
                    if (offerTb.Phone == "1")
                    {
                        offerTb.isToday = false;
                    }
                    else
                    {
                        offerTb.isToday = true;
                    }
                    db.OfferTBs.Add(offerTb);
                    db.SaveChanges();
                    return offerTb.Id;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
    }
}
