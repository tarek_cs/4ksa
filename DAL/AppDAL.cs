﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF;
using System.Net.Mail;

namespace DAL
{
    public static class AppDAL
    {
        public static AboutTB GetAboutApp()
        {
            using (var db = new KsaDBEntities())
            {
                return db.AboutTBs.Find(1);
            }
        }
        public static bool SendMailFromContact(string subject, string Email, string Msg)
        {
            using (var db = new KsaDBEntities())
            {
                var data = db.ContactTBs.Find(1);
                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = true;
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                // setup Smtp authentication
                System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential("contact@asami.com.sa", "Asami@2013");
                client.UseDefaultCredentials = false;
                client.Credentials = credentials;
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(Email, "MOBILE APP");
                msg.To.Add(new MailAddress(data.Email));
                msg.Subject = subject;
                msg.IsBodyHtml = true;
                msg.Body += "البريد الالكتروني  :" + Email + "<br/>";
                msg.Body += "تفاصيل الرسالة <br />" + Msg + "<br/>";
                try
                {
                    client.Send(msg);
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public static bool RatingApp(int User, int Rating)
        {
            using (var db = new KsaDBEntities())
            {
                try
                {
                    RatingTB modelTb = new RatingTB() { UserId = User, Rating = Rating };
                    db.RatingTBs.Add(modelTb);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {

                    return false;
                }
            }
        }
    }
}
