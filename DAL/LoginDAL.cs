﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Net;
using System.IO;
using EF;
using VM;
using System.Web.Security;


namespace DAL
{
    public class LoginDAL
    {
        public string tempKey = "ZWZ5^95wFlqZ";
        KsaDBEntities db = new KsaDBEntities();
        public int Authenticate(AccountModel accounttb, ref int Userid, ref string usermobile, ref int pr, ref string name, ref string userImg, ref string pname, ref string Email)
        {
            var userdata = db.UsersTBs.Where(i => (i.Mobile == accounttb.Mobile) && i.password == accounttb.Pass);

            if (userdata.Any() && userdata.Count() != 0)
            {
                if ((userdata.FirstOrDefault().Mobile == accounttb.Mobile) && userdata.FirstOrDefault().password == accounttb.Pass)// login success
                {
                    try
                    {
                        Userid = userdata.FirstOrDefault().id;
                        usermobile = userdata.FirstOrDefault().Mobile;
                        //pr = userdata.FirstOrDefault().pr;
                        //name = userdata.FirstOrDefault().name;
                        userImg = userdata.FirstOrDefault().image;
                        pr = userdata.FirstOrDefault().PlanId != null ? (int)userdata.FirstOrDefault().PlanId : 0;
                        Email = userdata.FirstOrDefault().email;
                        name = userdata.FirstOrDefault().name;
                        //if (userdata.FirstOrDefault().position != null)
                        //{
                        //    pname = userdata.FirstOrDefault().position.name;
                        //}
                        if (userdata.FirstOrDefault().isActive == true)
                        {
                            if (userdata.FirstOrDefault().isBlocked == false)
                            {
                                if (userdata.FirstOrDefault().PlanId != null)
                                {
                                    UsersTB userTb = userdata.FirstOrDefault();
                                    if (accounttb.RegistrationId != "null" && accounttb.RegistrationId != null)
                                    {
                                        userTb.isIos = false;
                                        userTb.RegistrationId = accounttb.RegistrationId;
                                        db.Entry(userTb).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                    return 1;
                                }
                                else
                                {
                                    return -5; //not choose plan
                                }
                            }
                            else
                            {
                                return -9;
                            }

                        }
                        else
                        {
                            return -10; //  this not active user
                        }
                    }
                    catch (Exception)
                    {
                        return 0;// error in save in network
                    }
                    //}
                    //else
                    //{
                    //    return -10; //  this not active user
                    //}
                }
                else
                {
                    return -1; //error in password
                }
            }
            else
            {
                return 0;// error in save in network
            }
        }
        public bool SaveRegistrationId(int userid, string regid)
        {
            using (var _db = new KsaDBEntities())
            {
                try
                {
                    if (regid != "null" && regid != null && regid != "")//update registration id
                    {
                        var userTb = _db.UsersTBs.Find(userid);
                        if (userTb != null)
                        {
                            userTb.RegistrationId = regid;
                            userTb.isIos = false;
                            _db.Entry(userTb).State = EntityState.Modified;
                            _db.SaveChanges();
                        }
                    }
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        public bool CheckCode(string id, string textCode)
        {
            if (id != "")
            {
                var userdata = db.UsersTBs.FirstOrDefault(i => i.Mobile == id);
                if (userdata.ConfirmCode == textCode)
                {
                    userdata.isActive = true;
                    db.UsersTBs.Attach(userdata);
                    db.Entry(userdata).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public bool ChoosePlan(int id, int plan)
        {

            try
            {
                var userdata = db.UsersTBs.Find(id);
                userdata.PlanId = plan;
                //db.UsersTBs.Attach(userdata);
                db.Entry(userdata).State = EntityState.Modified;
                db.SaveChanges();
                return true;

            }
            catch (Exception)
            {

                return false;
            }

        }
        public int CreateNewLogin(AccountModel accounttb, ref int userid, ref string usermobile)
        {
            try
            {

                UsersTB userobject = new UsersTB
                {
                    email = "",
                    password = accounttb.Pass,
                    name = "",
                    Mobile = accounttb.Mobile,
                    image = "http://koora.arabwin.com/areas/user-avatar-blue-96@2x.png",
                    list1 = 0,
                    list2 = 0,
                    type = 0,
                    isActive = false,
                    pr = 0,
                    isBlocked = false,
                    PositionFk = 0,
                };
                userobject.isIos = false;
                userobject.RegistrationId = accounttb.RegistrationId;
                //check user not here before
                var checkdata = db.UsersTBs.Where(i => i.Mobile.Trim() == accounttb.Mobile);
                if (!checkdata.Any())
                {
                    bool b = true;
                    string randomeCode = ReturenRandom();
                    string Res = SendSMS("966551555511", "aziz1234", "4KSA", accounttb.Mobile, 1, randomeCode);
                    if (Res == "1")
                    {
                        userobject.ConfirmCode = randomeCode;
                        userobject.isActive = false;
                        db.UsersTBs.Add(userobject);
                        db.SaveChanges();
                        usermobile = accounttb.Mobile;
                        userid = userobject.id;
                        return 1;
                    }
                    else
                    {
                        return -1; //couldn't send SMS
                    }
                }
                else
                {
                    var firstOrDefault = checkdata.FirstOrDefault();
                    if (firstOrDefault != null)
                    {
                        userid = firstOrDefault.id;
                    }
                    return -2;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public bool EditProfile(int id, string mobile, string name, string email, string code)
        {
            using (var db = new KsaDBEntities())
            {
                var data = db.UsersTBs.Find(id);
                if (code != null && code != "" && code != "null")
                {
                    if (code == data.ConfirmCode)
                    {
                        data.Mobile = mobile;
                        data.email = email;
                        data.name = name;
                        db.Entry(data).State = EntityState.Modified;
                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    string randomeCode = ReturenRandom();
                    string Res = SendSMS("966551555511", "aziz1234", "4KSA", data.Mobile, 0, randomeCode);
                    if (Res == "1")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
        public List<PlanTB> GetAllPlains()
        {
            using (var db = new KsaDBEntities())
            {
                db.Configuration.ProxyCreationEnabled = false;
                var data = db.PlanTBs.ToList();
                return data;
            }
        }
        public string SendSMS(string username, string password, string sender, string numbers, int type = 0, string randomeCode = "")
        {
            try
            {
                SMSCAPI obj = new SMSCAPI();
                string strPostResponse;
                strPostResponse = obj.SendSMS("4KSA", "49612414", numbers, randomeCode);
                if (strPostResponse == "SMS message(s) sent")
                {
                    return "1";
                }
                else
                {
                    return "0";
                }
                //HttpContext.Current.Response.Write("Server Response " + strPostResponse);
                
            }
            catch (Exception ex)
            {
                return "0";
            }
        }
        public string SendSMSCode(string username, string password, string sender, string numbers, string msg)
        {
            numbers = TrimPhone(numbers);
            if (numbers == "0")
                return numbers;
            //int temp = '0';
            HttpWebRequest req = (HttpWebRequest)
            WebRequest.Create("http://www.mobily.ws/api/msgSend.php");
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            string postData = "mobile=" + username + "&password=" + password + "&numbers=" + numbers + "&sender=" + sender + "&msg=" + ConvertToUnicode(msg) + "&applicationType=59";
            req.ContentLength = postData.Length;

            StreamWriter stOut = new
            StreamWriter(req.GetRequestStream(),
            System.Text.Encoding.ASCII);
            stOut.Write(postData);
            stOut.Close();
            // Do the request to get the response
            string strResponse;
            StreamReader stIn = new StreamReader(req.GetResponse().GetResponseStream());
            strResponse = stIn.ReadToEnd();
            stIn.Close();
            return strResponse;
        }

        public string getMD5(string value, string salt = "")
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(value + salt, "md5").ToLower();
        }

        private string ConvertToUnicode(string val)
        {
            string msg2 = string.Empty;

            for (int i = 0; i < val.Length; i++)
            {
                msg2 += convertToUnicode(System.Convert.ToChar(val.Substring(i, 1)));
            }

            return msg2;
        }

        private string convertToUnicode(char ch)
        {
            System.Text.UnicodeEncoding class1 = new System.Text.UnicodeEncoding();
            byte[] msg = class1.GetBytes(System.Convert.ToString(ch));

            return fourDigits(msg[1] + msg[0].ToString("X"));
        }
        private string fourDigits(string val)
        {
            string result = string.Empty;

            switch (val.Length)
            {
                case 1: result = "000" + val; break;
                case 2: result = "00" + val; break;
                case 3: result = "0" + val; break;
                case 4: result = val; break;
            }

            return result;
        }

        public string TrimPhone(string phone)
        {
            if (phone.StartsWith("966"))
            {
                return phone;
            }
            else
            {
                if (phone.StartsWith("05"))
                {
                    return "966" + phone.Substring(1, phone.Length - 1);
                }
                else
                {
                    if (phone.StartsWith("5") && phone.Length == 9)
                    {
                        return "966" + phone;
                    }
                    else
                    {
                        return "0";
                        //Interaction.MsgBox("خطأ في رقم الهاتف");
                    }
                }
            }
        }

        public bool SendCodeMAil(string Email, int type = 0)
        {
            try
            {
                string randomeCode = ReturenRandom();
                bool bcode = false;
                if (type == 0)
                {
                    bcode = UpdateUserCode(Email, randomeCode);
                }
                else
                {
                    bcode = UpdateUserCode2(Email, randomeCode);
                }
                if (bcode)
                {
                    SmtpClient client = new SmtpClient();
                    client.UseDefaultCredentials = false;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.EnableSsl = true;
                    client.Host = "smtp.gmail.com";
                    client.Port = 587;
                    // setup Smtp authentication
                    System.Net.NetworkCredential credentials =
                        new System.Net.NetworkCredential("mail@domain.com", "Password");
                    client.UseDefaultCredentials = false;
                    client.Credentials = credentials;
                    MailMessage msg = new MailMessage();
                    msg.From = new MailAddress("mail@fomain.com", "Title");
                    msg.To.Add(new MailAddress(Email));
                    msg.Subject = "Confirmation Code";
                    msg.IsBodyHtml = true;
                    msg.Body = randomeCode;
                    try
                    {
                        client.Send(msg);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            catch (Exception exception)
            {
                return false;
            }
        }

        //public int UpdateCodMail(string mail, string code, string newpass)
        //{
        //    var userData = db.UsersTBs.FirstOrDefault(i => i.id == mail);
        //    if (userData.isActive == true)
        //    {
        //     //   if (userData.ConfirmCode == code)
        //      //  {
        //            userData.password = newpass;
        //            db.UsersTBs.Attach(userData);
        //            db.Entry(userData).State = EntityState.Modified;
        //            db.SaveChanges();
        //            return 1;
        //    //    }
        //       // else
        //    //    {
        //        //    return 0;
        //       // }
        //    }
        //    else
        //    {
        //        return -1;
        //    }
        //}
        public bool UpdateUserCodeByMobile(string mobile, string code)
        {
            if (mobile != "")
            {
                var userdata = db.UsersTBs.FirstOrDefault(i => i.Mobile == mobile);
                if (userdata != null)
                {
                    userdata.ConfirmCode = code;
                    db.UsersTBs.Attach(userdata);
                    db.Entry(userdata).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public bool UpdateUserCode(string mobile, string code)
        {
            if (mobile != "")
            {
                var userdata = db.UsersTBs.FirstOrDefault(i => i.Mobile == mobile);
                if (userdata != null)
                {
                    userdata.ConfirmCode = code;
                    db.UsersTBs.Attach(userdata);
                    db.Entry(userdata).State = EntityState.Modified;
                }
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool UpdateUserCode2(string mail, string code)
        {
            if (mail != "")
            {
                var userdata = db.UsersTBs.FirstOrDefault(i => i.email == mail);
                if (userdata != null)
                {
                    userdata.ConfirmCode = code;
                    //userdata.isActive = false;
                    db.UsersTBs.Attach(userdata);
                    db.Entry(userdata).State = EntityState.Modified;
                }
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        //public bool SendContactMe(int userid, string message, string subject, string type)
        //{
        //    var userdata = db.UsersTBs.Find(userid);
        //    return SendMailToAdmin(userdata, message, subject, type);
        //}
        //public bool SendMailToAdmin(user userTb, string message, string subject, string type)
        //{
        //    SmtpClient client = new SmtpClient();
        //    client.UseDefaultCredentials = false;
        //    client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //    client.EnableSsl = false;
        //    client.Host = "mail.kooraapp.com";
        //    client.Port = 25;
        //    // setup Smtp authentication
        //    System.Net.NetworkCredential credentials =
        //        new System.Net.NetworkCredential("info@kooraapp.com", "411A[5e1_9m@M_R");
        //    client.UseDefaultCredentials = false;
        //    client.Credentials = credentials;
        //    MailMessage msg = new MailMessage();
        //    msg.From = new MailAddress(userTb.email, userTb.name);
        //    msg.To.Add(new MailAddress(db.strings.Find(43).value));
        //    msg.Subject = subject;
        //    msg.IsBodyHtml = true;
        //    msg.Body = type + "<br/><br/>" + msg;
        //    try
        //    {
        //        client.Send(msg);
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //public IEnumerable<ContactTypeTB> GetContactType()
        //{
        //    db.Configuration.ProxyCreationEnabled = false;
        //    return db.ContactTypeTBs.ToArray();
        //}
        public bool SendMailRegisterUser(string mail, string pass)
        {
            try
            {
                //  string randomeCode = ReturenRandom();
                //  UpdateUserCode(Email, randomeCode);
                SmtpClient client = new SmtpClient();
                client.UseDefaultCredentials = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = true;
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                // setup Smtp authentication
                System.Net.NetworkCredential credentials =
                    new System.Net.NetworkCredential("mail@domain.om", "password");
                client.UseDefaultCredentials = false;
                client.Credentials = credentials;
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress("mail@domain.om", "koora app");
                msg.To.Add(new MailAddress(mail));
                msg.Subject = "Support";
                msg.IsBodyHtml = true;
                msg.Body = "body";
                try
                {
                    client.Send(msg);
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            catch (Exception exception)
            {
                return false;
            }
        }

        public string ReturenRandom()
        {
            Random r = new Random();
            var x = r.Next(0, 1000000);
            string code = x.ToString("0000");
            return code;
        }


        public bool UpdateMobile(int id, string mobile)
        {
            try
            {
                var usertb = db.UsersTBs.Find(id);
                usertb.Mobile = mobile;
                //  db.UsersTBs.Attach(usertb);
                db.Entry(usertb).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdatePassword(string newPass, int userId)
        {
            var userData = db.UsersTBs.Find(userId);
            if (userData != null)
            {
                userData.password = newPass;
                db.UsersTBs.Attach(userData);
                db.Entry(userData).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ResetPasswordCode(string mobile)
        {
            try
            {
                string randomeCode = ReturenRandom();
                bool b = UpdateUserCodeByMobile(mobile, randomeCode);
                if (b)
                {
                    string Res = SendSMSCode("966551555511", "aziz1234", "Koora App", mobile, randomeCode);
                    return true;
                }
                else
                {
                    return b;
                }
            }
            catch (Exception)
            {
                return false;
            }
            //2- send to mobile
        }

        public bool ResetPassword(string mobile, string code, string password)
        {
            try
            {
                var userdata = db.UsersTBs.Where(x => x.Mobile == mobile).FirstOrDefault();
                if (userdata.ConfirmCode == code)
                {
                    userdata.password = password;
                    db.Entry(userdata).State = EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public string getMd5Hash(string input)
        { // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create(); // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            // Create a new Stringbuilder to collect the bytes // and create a string.
            StringBuilder sBuilder = new StringBuilder(); // Loop through each byte of the hashed data // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
        //public bool RegisterCourseForUser(int userid, int formid)
        //{
        //    var data = db.CoursesTBs.FirstOrDefault(i => i.FormFk == formid);
        //    if (data != null)
        //    {
        //        UsersCoursesTB usercoursetb = new UsersCoursesTB();
        //        usercoursetb.UserFK = userid;
        //        usercoursetb.CourseFK = data.CoursesID;
        //        db.UsersCoursesTBs.Add(usercoursetb);
        //        db.SaveChanges();
        //        CoursesBLL obj = new CoursesBLL();
        //        obj.SendMailToManagers(data.CoursesID, userid);
        //        obj.SendClientMail(userid);
        //      //  ContactBLL objcontact=new ContactBLL();.
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
    }
}